-- TODO make timestamps updateable 
-- TODO add "Files" table
-- TODO add "Schedules table"
CREATE DATABASE ccio WITH ENCODING 'UTF8';
\connect ccio;

CREATE TABLE IF NOT EXISTS "API" (
  ke varchar(50) DEFAULT NULL,
  uid varchar(50) DEFAULT NULL,
  ip text,
  code varchar(100) DEFAULT NULL,
  details text,
  time timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP 
) ;

CREATE TABLE IF NOT EXISTS "Cloud Videos" (
  mid varchar(50) NOT NULL,
  ke varchar(50) DEFAULT NULL;
  href text NOT NULL,
  size float DEFAULT NULL,
  time timestamp NULL DEFAULT NULL,
  "end" timestamp NULL DEFAULT NULL,
  status numeric(1) DEFAULT '0',
  details text
) ;
COMMENT ON COLUMN "Cloud Videos".status IS '0:Complete,1:Read,2:Archive';

CREATE TABLE IF NOT EXISTS "Events" (
  ke varchar(50) DEFAULT NULL,
  mid varchar(50) DEFAULT NULL,
  details text,
  time timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP 
)  ;

CREATE TABLE IF NOT EXISTS "Logs" (
  ke varchar(50) DEFAULT NULL,
  mid varchar(50) DEFAULT NULL,
  info text,
  time timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP 
) ;

CREATE TABLE IF NOT EXISTS "Monitors" (
  mid varchar(50) DEFAULT NULL,
  ke varchar(50) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  shto text,
  shfr text,
  details text,
  type varchar(50) DEFAULT 'jpeg',
  ext varchar(50) DEFAULT 'webm',
  protocol varchar(50) DEFAULT 'http',
  host varchar(100) DEFAULT '0.0.0.0',
  path varchar(100) DEFAULT '/',
  port int DEFAULT '80',
  fps int DEFAULT '1',
  mode varchar(15) DEFAULT NULL,
  width int DEFAULT '640',
  height int DEFAULT '360'
) ;

CREATE TYPE preset_enum AS ENUM ('monitor','event','user');
CREATE TABLE IF NOT EXISTS "Presets" (
  ke varchar(50) DEFAULT NULL,
  name text,
  details text,
  type preset_enum DEFAULT NULL
) ;

CREATE TABLE IF NOT EXISTS "Users" (
  ke varchar(50) DEFAULT NULL,
  uid varchar(50) DEFAULT NULL,
  auth varchar(50) DEFAULT NULL,
  mail varchar(100) DEFAULT NULL,
  pass varchar(100) DEFAULT NULL,
  details text,
  CONSTRAINT mail UNIQUE  (mail)
) ;

CREATE TYPE ext_enum AS ENUM ('webm','mp4');
CREATE TABLE IF NOT EXISTS "Videos" (
  mid varchar(50) DEFAULT NULL,
  ke varchar(50) DEFAULT NULL,
  ext ext_enum DEFAULT NULL,
  time timestamp(0) NULL DEFAULT NULL,
  duration double precision DEFAULT NULL,
  size double precision DEFAULT NULL,
  frames int DEFAULT NULL,
  "end" timestamp(0) NULL DEFAULT NULL,
  status int DEFAULT '0' ,
  details text
) ;
